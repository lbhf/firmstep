# LBHF Firmstep mplementation

## Overview
This document describes LBHF Firmstep implementation processes, recommendations, and things to watch out for.

## Code

### Caching
Note that it may take between several minutes and an hour or more for changes to become visible onces pushed to the PROD branch.


### Assets
The ```/public``` folder contains the assets needed by Firmstep. Every time you make a change to the PROD branch of this repo, the content of the ```/public``` folder is deployed to [https://lbhf.gitlab.io/firmstep/](https://lbhf.gitlab.io/firmstep/).

Folder structure:
* ```/public/css```: contains the css files.
* ```/public/images```: contains images - most importantly the LBHF logo in png, jpg, and svg format.
* ```/public/imates/icons```: contains a copy of the svg icons used on lbhf.gov.uk.

## Firmstep configuration

### Logo
You can upload a logo at **Firmstep > Self admin MyServices >  Content/Translations > Page Components > General**, but we currently do NOT use it because the ```{SiteLogo}``` token is not available where we need it.

Instead we insert the logo in the header and footer as follows:

```
<a href="{SiteTitleUrl}">
  <img src="https://lbhf.gitlab.io/firmstep/images/logo-LBHF.svg" alt="Hammersmith &amp; Fulham">
</a>
```

### Navigation

#### Toolbar

Defined at **Firmstep > Self admin MyServices >  Content/Translations > Page Components > Toolbar**.

#### Top-level Navigation

You can change the *labels* of the navigation items at **Firmstep > Self admin MyServices >  Content/Translations > Page Components > Navigation**.

You can change the *order* of the navigation items at **Firmstep > Self admin MyServices >  Navigation Items**.
