# Firmstep assets

## Overview
This repository contains assets used by Firmstep to style LBHF's Firmstep implementation.

The deployed files are reachable at [https://lbhf.gitlab.io/firmstep](https://lbhf.gitlab.io/firmstep).

Examples:
* ```public/css/lbhf.css``` => https://lbhf.gitlab.io/firmstep/css/lbhf.css

* ```public/images/logo-LBHF.svg``` => https://lbhf.gitlab.io/firmstep/images/logo-LBHF.svg

## Files
```public/css/lbhf.css```
* an exact copy of the main css file from lbhf.gov.uk.
* be sure to update this file when the css from lbhf.gov.uk is updated.

```public/css/firmstep.css```
* the custom css that overrides Firmstep's Bootstrap css where necessary.

## Developing and deploying
When you need to make changes to the code, create a new branch (branched from MASTER). When you're ready to deploy, merge your branch into MASTER, and then into PROD.

As soon as you merge into PROD, a Gitlab CI job will automatically deploy the changes to [https://lbhf.gitlab.io/firmstep](https://lbhf.gitlab.io/firmstep).

The job itself may take up to a minute; Gitlab may take a few minutes or more to clear its caches.

## More documentation

See ```/docs/README.md``` for further documentation.